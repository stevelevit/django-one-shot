from django.shortcuts import render, get_object_or_404
from todos.models import TodoList


def todo_list_view(request):
    todo_lists = TodoList.objects.all()
    return render(request, 'todos/todo_list.html', {'todo_lists':todo_lists})

def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    return render(request, 'todos/details.html', {'todo_list':todo_list})
